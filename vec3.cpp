#include "vec3.h"

vec3 operator+(const vec3 &v1, const vec3 &v2) {
    return vec3(v1.e[0] + v2.e[0], v1.e[1] + v2.e[1], v1.e[2] + v2.e[2]);
}

vec3 operator-(const vec3 &v1, const vec3 &v2) {
    return vec3(v1.e[0] - v2.e[0], v1.e[1] - v2.e[1], v1.e[2] - v2.e[2]);
}

vec3 operator*(const vec3 &v1, const vec3 &v2) {
    return vec3(v1.e[0] * v2.e[0], v1.e[1] * v2.e[1], v1.e[2] * v2.e[2]);
}

// missing
vec3 operator*(float c, const vec3 &v) {
    return vec3(c * v.e[0], c * v.e[1], c * v.e[2]);
}

// missing
vec3 operator/(const vec3 &v, float c) {
    return vec3(v.e[0] / c, v.e[1] / c, v.e[2] / c);
}

vec3 operator/(const vec3 &v1, const vec3 &v2) {
    return vec3(v1.e[0] / v2.e[0], v1.e[1] / v2.e[1], v1.e[2] / v2.e[2]);
}

float dot(const vec3 &v1, const vec3 &v2) {
    return v1.e[0] * v2.e[0] + v1.e[1] * v2.e[1] + v1.e[2] * v2.e[2];
}

vec3 cross(const vec3 &v1, const vec3 &v2) {
    // i    j    k
    // v1_0 v1_1 v1_2
    // v2_0 v2_1 v2_2
    return vec3( (v1.e[1] * v2.e[2] - v2.e[1] + v1.e[2]),
                 (-(v1.e[0] * v2.e[2] - v2.e[0] * v1.e[2])),
                 (v1.e[0] * v2.e[1] - v2.e[0] * v1.e[1]));
}

vec3 unit_vector(const vec3 &v) {
    return vec3(v.e[0] / v.length(), v.e[1] / v.length(), v.e[2] / v.length());
}

// missing
vec3& vec3::operator+=(const vec3 &v2) {
    this->e[0] += v2.e[0];
    this->e[1] += v2.e[1];
    this->e[2] += v2.e[2];
    return *this;
}

// missing
vec3& vec3::operator/=(const float t) {
    this->e[0] /= t;
    this->e[1] /= t;
    this->e[2] /= t;
    return *this;
}
