#include <QCommandLineParser>
#include <QDesktopServices>
#include <QDir>
#include <QFile>
#include <QGuiApplication>
#include <QtDebug>
#include <QTextStream>
#include <QTime>
#include <QUrl>
#include <QtConcurrent>

#include "sphere.h"
#include "hitable_list.h"
#include "float.h"
#include "camera.h"

const int nx = 800;
const int ny = 400;
const int ns = 20;

vec3 random_in_unit_sphere() {
    vec3 p;
    do {
        float xx = (rand() / (RAND_MAX + 1.0));
        float yy = (rand() / (RAND_MAX + 1.0));
        float zz = (rand() / (RAND_MAX + 1.0));
        p = 2.0 * vec3(xx, yy, zz) - vec3(1, 1, 1);
    } while (dot(p, p) >= 1.0);

    return p;
}

vec3 color(const ray& r, hitable *world) {

    hit_record rec;

    if (world->hit(r, 0.001, FLT_MAX, rec))
    {
        // Any object
        vec3 target = rec.p + rec.normal + random_in_unit_sphere();
        return 0.5 * color(ray(rec.p, target-rec.p), world);
    } else {
        // Sky
        vec3 unit_direction = unit_vector(r.direction());
        float t = 0.5 * (unit_direction.y() + 1.0);
        return (1.0-t) * vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
    }
}

void render_row(QList<int> &row, camera cam, hitable *world) {

    int j = row.takeFirst();

    j = (ny - j);

    for (int i = 0; i < nx; i++) {

        vec3 col(0, 0, 0);

        for (int s = 0; s < ns; ++s) {

            // displacement
            float d = (s / float(ns)) * 2 - 1;

            float u = float(i + d) / float(nx);
            float v = float(j + d) / float(ny);

            ray r = cam.get_ray(u, v);
            col += color(r, world);
        }

        col /= float(ns);

        int ir = int(255.99 * sqrt(col.r()));
        int ig = int(255.99 * sqrt(col.g()));
        int ib = int(255.99 * sqrt(col.b()));

        row.append(ir);
        row.append(ig);
        row.append(ib);
    }
}

int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription("Test helper");
    QCommandLineOption outputOpt("o", "Output file");
    parser.addOption(outputOpt);

    parser.process(app);

    const QStringList args = parser.positionalArguments();

    QFile output_file(args.at(0));
    output_file.open(QIODevice::WriteOnly);

    QTextStream out(&output_file);

    out << "P3\n" << nx << " " << ny << "\n255\n";

    hitable *list[2];
    list[0] = new sphere(vec3(0, 0, -1), 0.5);
    list[1] = new sphere(vec3(0, -100.5, -1), 100);
    hitable *world = new hitable_list(list, 2);

    camera cam;

    QTime time;
    time.start();

    QList<QList<int>> out_px;
    for (int j = 0; j < ny; ++j) {
        QList<int> row;

        // Have each row know its index
        row.append(j);

        out_px.append(row);
    }

    std::function<void(QList<int>&)> render_map = [cam, world](QList<int> &row) {
        return render_row(row, cam, world);
    };

    QFuture<void> future = QtConcurrent::map(out_px, render_map);

    future.waitForFinished();

    qDebug() << time.elapsed() << "ms!";

    for (int l = 0; l < out_px.count(); l++) {
        QList<int> row = out_px.at(l);

        for (int p = 0; p < row.count(); p += 3) {
            out << row.at(p) << " ";
            out << row.at(p+1) << " ";
            out << row.at(p+2) << "\n";
        }
    }

    QString path = QDir::currentPath() + "/" + output_file.fileName();
    QUrl url = QUrl::fromLocalFile(path);
    QDesktopServices::openUrl(url);

    return 0;
}
