QT += core
QT += gui
QT += concurrent

CONFIG += c++11

TARGET = pantaray
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    sphere.cpp \
    vec3.cpp \
    hitable_list.cpp \
    camera.cpp

HEADERS += \
    vec3.h \
    ray.h \
    hitable.h \
    sphere.h \
    hitable_list.h \
    camera.h

RESOURCES +=

DISTFILES +=
